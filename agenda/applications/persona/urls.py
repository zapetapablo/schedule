from django.urls import path, re_path, include
from . import views
app_name = "personApp"

urlpatterns = [
    path('personas/', views.ListaPersonas.as_view(), name="people"),
    path('api/persona/lista/', views.PersonListAPIView.as_view()),
    path('personaTemplate/', views.PersonTemplateView.as_view(),name="lista"),
    path('api/persona/search/<kword>/', views.PersonSearchApiView.as_view(), name="searchForKey"),
    path('api/persona/crear/', views.PersonCreateView.as_view(), name="createPerson"),
    path('api/persona/detalle/<pk>/', views.PersonDetailAPIview.as_view(), name="detailPerson"),
    path('api/persona/borrar/<pk>/', views.PersonDeleteView.as_view(), name="deletePerson"),
    path('api/persona/actualizar/<pk>/', views.PersonUpdateView.as_view(), name="updatePerson"),
    path('api/persona/modificar/<pk>/', views.PersonModifyView.as_view(), name="ModifyPerson"),
]
