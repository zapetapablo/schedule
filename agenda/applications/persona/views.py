from django.shortcuts import render

from django.views.generic import (
    ListView,
    TemplateView,
    CreateView
)

from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    UpdateAPIView,
    RetrieveUpdateAPIView,
)

from .models import Person
from .serializers import PersonSerializer


class ListaPersonas(ListView):
    template_name = "persona/personList.html"
    context_object_name = "person_list"

    def get_queryset(self):
        return Person.objects.all()


class PersonListAPIView(ListAPIView):
    serializer_class = PersonSerializer
    def get_queryset(self):
        return Person.objects.all()


class PersonTemplateView(TemplateView):
    template_name = "persona/lista.html"


class PersonSearchApiView(ListAPIView):
    serializer_class = PersonSerializer

    def get_queryset(self):
        #filtramos datos
        kword = self.kwargs["kword"]
        return Person.objects.filter(
            full_name__icontains = kword
        )


class PersonCreateView(CreateAPIView):
    serializer_class =  PersonSerializer


class PersonDetailAPIview(RetrieveAPIView):

    serializer_class = PersonSerializer
    queryset = Person.objects.filter()


class PersonDeleteView(DestroyAPIView):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()


class PersonUpdateView(UpdateAPIView):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()


class PersonModifyView(RetrieveUpdateAPIView):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()

